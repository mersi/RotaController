all: RotaControllerTest.exe

clean:
	rm libRotaController.a RotaController.exe

libRotaController.a: RotaController.cc RotaController.h
	$(CXX) -fmax-errors=1 -std=c++11 -I/usr/local/PI/include -fPIC -c RotaController.cc -o libRotaController.a

RotaControllerTest.exe: RotaControllerTest.cc libRotaController.a
	$(CXX) -std=c++11 libRotaController.a -lpi_pi_gcs2 RotaControllerTest.cc -o RotaController.exe
